## Sky Bet Tech Test

A live football application built with React, Redux & ES6.

## Key Technology / Tools

* Create-react-app
* HTML5, SASS, ES6
* Redux
* HTTP - Axios
* Test - Jest, Enzyme, Moxios

## How To Use

The direct BitBucket link to this project is: https://bitbucket.org/tblaymire/sky-bet-test/

To clone and run this application, you'll need [Git](https://git-scm.com) and [Node.js](https://nodejs.org/en/download/) (which comes with [npm](http://npmjs.com)) installed on your computer. From your command line:

```bash
# Clone this repository
$ git clone https://tblaymire@bitbucket.org/tblaymire/sky-bet-test.git

# Go into the repository
$ cd sky-bet-test

# Install dependencies
$ npm install

# Run the app
$ npm run start or $ yarn run start
```

Note: If you're using Linux Bash for Windows, [see this guide](https://www.howtogeek.com/261575/how-to-run-graphical-linux-desktop-applications-from-windows-10s-bash-shell/) or use `node` from the command prompt.

## Developer Notes / Choices

* Using Sass due to the nature of the small project. However larger projects CSS modules are reccomended.
* One downside of using create-react-app and sass is using npm-eject meaning create-react-app updates will not be applied.
* Non completed features such as toogle and full details due to time. (toggle does use localStorage for this see index.js)
* Unit testing failing / not complete, however base setup is incldued.
