import React from 'react';
import './LiveScoreHeader.scss';

const liveScoreHeader = props => {
	return (
		<div className="live-score-header">
			<div className="live-score-header__content">
				<span className="live-score__team-a">
					<h3>Leeds United</h3>
				</span>

				<span className="live-score__result">5 : 0</span>

				<span className="live-score__team-b">
					<h3>Manchester United</h3>
				</span>
			</div>
		</div>
	);
};

export default liveScoreHeader;
