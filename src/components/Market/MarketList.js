import React from 'react';
import MarketItem from './MarketItem/MarketItem';

const marketList = props => {
	let marketItems = null;
	if (props.markets) {
		marketItems = props.markets.map(market => (
			<MarketItem key={market.marketId} type={market.type} name={market.name} />
		));
	}
	return <div className="markets-list">{marketItems}</div>;
};

export default marketList;
