import React from 'react';
import './MarketItem.scss';

const marketItem = props => {
	return (
		<div className="market-container">
			<details className="market-item__details">
				<summary className="market-item" onClick={props.toggleMarketItem}>
					{props.name}
				</summary>
				<p className="market-item__content">
					<span>Type: {props.type}</span>
				</p>
			</details>
		</div>
	);
};

export default marketItem;
