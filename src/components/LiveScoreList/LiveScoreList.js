import React from 'react';
import LiveScoreItem from './LiveScoreItem/LiveScoreItem';
import './LiveScoreList.scss';

const liveScoreList = props => {
	let eventItems = null;
	if (props.events) {
		eventItems = props.events.map(event => (
			<LiveScoreItem
				key={event.eventId}
				id={event.eventId}
				name={event.name}
				scores={event.scores}
				homeTeam={event.competitors[0].name}
				awayTeam={event.competitors[1].name}
				homeScore={event.scores.home}
				awayScore={event.scores.away}
				startTime={event.startTime}
				league={event.linkedEventTypeName}
				live={event.live}
				primaryMarketActive={props.primaryMarketActive}
				getMarketDetails={props.getMarketDetails}
				markets={props.markets}
			/>
		));
	}

	return <div className="live-score-list">{eventItems}</div>;
};

export default liveScoreList;
