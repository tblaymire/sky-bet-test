import React from 'react';
import { Link } from 'react-router-dom';
import marketIcon from '../../../assets/ticket.svg';
import './LiveScoreItem.scss';

const scoreListItem = props => {
	let primaryMarket = null;
	if (props.markets[0]) {
		let singleMarket = props.markets[0].name;
		primaryMarket = <div className="primary-market">{singleMarket}</div>;
	}

	return (
		<div>
			<span
				className="live-score-market"
				onClick={() => {
					props.getMarketDetails(props.id);
				}}
			>
				<img src={marketIcon} alt="Primary Market" />
			</span>
			<div>
				<Link to={'/football-live/' + props.id} className="live-score-item__link">
					<div className={'live-score-item ' + (props.primaryMarketActive ? 'live-score-item--expanded' : '')}>
						<div className="live-score-competitors">
							<h3 className="live-score-team__name live-score-team__name--a">{props.homeTeam}</h3>

							<div className="live-score-result">
								<span className="live-score-result-home__value">{props.homeScore}</span>
								<span className="live-score-result__spacer">-</span>
								<span className="live-score-result-away__value">{props.awayScore}</span>
							</div>

							<h3 className="live-score-team__name live-score-team__name--b">{props.awayTeam}</h3>

							{primaryMarket}
						</div>
					</div>
				</Link>
			</div>
		</div>
	);
};

export default scoreListItem;
