import React from 'react';
import './ToggleUnit.scss';

const toggleUnit = props => {
  return (
    <div className="toggleWrapper">
      <input
        type="checkbox"
        className="dn"
        id="dn"
        onClick={props.toggleClick}
      />
      <label htmlFor="dn" className="toggle">
        <span className="toggle__handler" />
      </label>
    </div>
  );
};

export default toggleUnit;
