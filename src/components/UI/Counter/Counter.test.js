import React from 'react';
import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import Toggle from './Toggle';

configure({ adapter: new Adapter() });

describe('<Toggle />', () => {
  it('should contain an input field', () => {
    const wrapper = shallow(<Toggle />);
    expect(wrapper.find('input'));
  });

  it('should toggle the inputs state when clicked', () => {
    const wrapper = shallow(<Toggle />);
    const toggle = wrapper.find('input');

    const toggleClick = toggle.onChange();
    const spy = jest.spyOn(toggle, 'toggleClick');

    expect(spy).toHaveBeenCalled();
    expect(toggleClick).toBe(true);

    spy.mockReset();
    spy.mockRestore();
  });
});
