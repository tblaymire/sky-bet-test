import React from 'react';
import './Counter.scss';

const counter = props => {
  return (
    <div className="counter">
      <span className="counter__number">{props.countValue}</span>
      <span className="counter__title">Live Matches</span>
    </div>
  );
};

export default counter;
