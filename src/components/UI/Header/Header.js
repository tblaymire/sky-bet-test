import React from 'react';
import './Header.scss';

const header = props => (
  <header className="header">
    <h3 className="header__title">{props.title}</h3>
    <p className="header__subtitle">{props.description}</p>
  </header>
);

export default header;
