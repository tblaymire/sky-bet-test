import React from 'react';
import { mount, configure } from 'enzyme';
import Header from './Header';
import Adapter from 'enzyme-adapter-react-16';

configure({ adapter: new Adapter() });

describe('<Header />', () => {
  let props;
  let mountedHeader;

  const header = () => {
    if (!mountedHeader) {
      mountedHeader = mount(<Header {...props} />);
    }
    return mountedHeader;
  };

  beforeEach(() => {
    props = {
      title: undefined,
      description: undefined
    };
    mountedHeader = undefined;
  });

  it('should show a header title from props', () => {
    const header = header().find(Header);
    expect(Object.keys(Header.props()).length).toBe(1);
  });
});
