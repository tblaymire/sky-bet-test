import React, { Component } from 'react';
import LiveScoreList from '../../../containers/LiveScoreListContainer/LiveScoreListContainer';
import Toggle from '../../../containers/ToggleUnitContainer/ToggleUnitContainer';
import Counter from '../../../containers/CounterContainer/CounterContainer';
import Header from '../../../components/UI/Header/Header';
import './LiveFootballPage.scss';

class LiveFootballPage extends Component {
	constructor(props) {
		super(props);
		this.state = {
			pageTitle: 'Live Football',
			pageDescription: 'Enjoy all the thrills of the Barclays Premier League and place a bet with a touch of a button.',
		};
	}
	render() {
		return (
			<div className="live-football">
				<Header title={this.state.pageTitle} description={this.state.pageDescription} />
				<div className="page-container">
					<div className="live-football__options">
						<Counter />
						<Toggle />
					</div>
					<LiveScoreList />
				</div>
			</div>
		);
	}
}

export default LiveFootballPage;
