import React, { Component } from 'react';
import './LiveFootballSinglePage.scss';
import LiveScoreSingle from '../../../containers/LiveScoreSingleContainer/LiveScoreSingleContainer';
import Header from '../../../components/UI/Header/Header';
import MarketList from '../../../containers/MarketContainer/MarketContainer';

class LiveFootballSinglePage extends Component {
	constructor(props) {
		super(props);
		this.state = {
			pageTitle: 'Live Event',
			pageDescription: 'To bet on a single market please click on a category below.',
		};
	}
	render() {
		return (
			<div className="live-football-single">
				<Header title={this.state.pageTitle} description={this.state.pageDescription} />
				<LiveScoreSingle />
				<div className="page-container">
					<h3>Betting Categories</h3>
					<MarketList />
				</div>
			</div>
		);
	}
}

export default LiveFootballSinglePage;
