import React, { Component } from 'react';
import { BrowserRouter, Route, Switch, Redirect } from 'react-router-dom';
import Layout from './hoc/Layout/Layout';
import LiveFootballPage from './components/Pages/LiveFootball/LiveFootballPage';
import LiveFootballSinglePage from './components/Pages/LiveFootballSingle/LiveFootballSinglePage';

class App extends Component {
	render() {
		return (
			<div>
				<Layout>
					<BrowserRouter>
						<Switch>
							<Route path="/football-live" exact component={LiveFootballPage} />
							<Route path="/football-live/:id" component={LiveFootballSinglePage} />
							<Redirect path="/" to="/football-live" />
						</Switch>
					</BrowserRouter>
				</Layout>
			</div>
		);
	}
}

export default App;
