import * as actionTypes from '../actions/actionTypes';

const initialState = {
	markets: [],
	id: null,
	fetching: false,
};

const loadMarketSuccess = (state, action) => {
	return Object.assign({}, state, {
		markets: action.markets,
		id: action.id,
	});
};

const loadMarketFailure = (state, action) => {
	return {
		...state,
		error: action.error,
	};
};

const reducer = (state = initialState, action) => {
	switch (action.type) {
		case actionTypes.LOAD_PRIMARY_MARKET_SUCCESS:
			return loadMarketSuccess(state, action);

		case actionTypes.LOAD_PRIMARY_MARKET_FAILURE:
			return loadMarketFailure(state, action);

		default:
			return state;
	}
};

export default reducer;
