import reducer from './events';
import * as actionTypes from '../actions/actionTypes';

describe('events reducer', () => {
  it('should return the initial state', () => {
    expect(reducer(undefined, {})).toEqual({
      events: [],
      loading: false,
      toggleStatus: false,
      error: null
    });
  });

  it('should store the events upon success', () => {
    expect(
      reducer(
        {
          events: [],
          loading: false,
          toggleStatus: false,
          error: null
        },
        {
          type: actionTypes.LOAD_EVENTS_DATA_SUCCESS,
          events: 'some array',
          loading: false
        }
      )
    ).toEqual({
      events: 'some array',
      loading: false,
      toggleStatus: false,
      error: null
    });
  });

  it('should return an error upon failure', () => {
    expect(
      reducer(
        {
          events: [],
          loading: false,
          error: null,
          toggleStatus: false
        },
        {
          type: actionTypes.LOAD_EVENTS_DATA_FAILURE,
          loading: false,
          error: 'some error'
        }
      )
    ).toEqual({
      events: [],
      loading: false,
      error: 'some error',
      toggleStatus: false
    });
  });
});
