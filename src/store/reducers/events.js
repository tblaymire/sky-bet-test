import * as actionTypes from '../actions/actionTypes';

const initialState = {
  events: [],
  loading: false,
  toggleStatus: false,
  error: null
};

const loadEventsStart = (state, action) => {
  return {
    ...state,
    error: null,
    loading: true
  };
};

const loadEventsSuccess = (state, action) => {
  return Object.assign({}, state, {
    events: action.events,
    loading: false
  });
};

const loadEventsFailure = (state, action) => {
  return {
    ...state,
    error: action.error,
    loading: false
  };
};

const toggleUnitValue = (state, action) => {
  return {
    ...state,
    toggleStatus: !state.toggleStatus
  };
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.LOAD_EVENTS_DATA_START:
      return loadEventsStart(state, action);

    case actionTypes.LOAD_EVENTS_DATA_SUCCESS:
      return loadEventsSuccess(state, action);

    case actionTypes.LOAD_EVENTS_DATA_FAILURE:
      return loadEventsFailure(state, action);

    case actionTypes.TOGGLE_UNIT:
      return toggleUnitValue(state, action);

    default:
      return state;
  }
};

export default reducer;
