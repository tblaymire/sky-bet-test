import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import moxios from 'moxios';
import { test, expect } from 'jest';
import * as actions from '../actions/actionTypes';
import getEventsMock from '../../mocks/getEventsMock';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('loadEventsData actions', () => {
	beforeEach(function() {
		moxios.install();
	});

	afterEach(function() {
		moxios.uninstall();
	});

	it('creates LOAD_EVENTS_DATA_SUCCESS after successfuly fetching events', () => {
		moxios.wait(() => {
			const request = moxios.requests.mostRecent();
			request.respondWith({
				status: 200,
				response: getEventsMock,
			});
		});

		const expectedActions = [
			{ type: actions.LOAD_EVENTS_DATA_START },
			{ type: actions.LOAD_EVENTS_DATA_SUCCESS, events: getEventsMock },
		];

		const store = mockStore({ events: [] });

		return store.dispatch(actions.getPosts()).then(() => {
			// return of async actions
			expect(store.getActions()).toEqual(expectedActions);
		});
	});
});
