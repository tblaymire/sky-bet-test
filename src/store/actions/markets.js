import * as actionTypes from '../actions/actionTypes';
import axios from 'axios';

export const loadMarketSuccess = (markets, id) => {
	return {
		type: actionTypes.LOAD_PRIMARY_MARKET_SUCCESS,
		markets: markets,
		id: id,
	};
};

export const loadPrimaryMarketFailure = error => {
	return {
		type: actionTypes.LOAD_PRIMARY_MARKET_FAILURE,
		error: error,
	};
};

export const loadPrimaryMarket = id => {
	let newId = id.toString();
	return dispatch => {
		axios
			.get(`http://localhost:8888/sportsbook/event/${id}`)
			.then(response => {
				dispatch(loadMarketSuccess(response.data.markets[newId]));
			})
			.catch(error => {
				dispatch(loadPrimaryMarketFailure(error));
			});
	};
};
