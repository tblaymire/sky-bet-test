import * as actionTypes from '../actions/actionTypes';
import axios from 'axios';

export const loadEventsStart = () => {
	return {
		type: actionTypes.LOAD_EVENTS_DATA_START,
	};
};

export const loadEventsSuccess = events => {
	return {
		type: actionTypes.LOAD_EVENTS_DATA_SUCCESS,
		events: events,
	};
};

export const loadEventsFailure = error => {
	return {
		type: actionTypes.LOAD_EVENTS_DATA_FAILURE,
		error: error,
	};
};

export const loadEventsData = () => {
	return dispatch => {
		dispatch(loadEventsStart());
		axios
			.get('http://localhost:8888/football/live')
			.then(response => {
				dispatch(loadEventsSuccess(response.data.events));
			})
			.catch(error => {
				dispatch(loadEventsFailure(error));
			});
	};
};

export const updateToggleUnit = () => {
	return {
		type: actionTypes.TOGGLE_UNIT,
	};
};
