// Scores Action Types
export const LOAD_EVENTS_DATA_START = 'LOAD_EVENTS_START';
export const LOAD_EVENTS_DATA_SUCCESS = 'LOAD_EVENTS_DATA_SUCCESS';
export const LOAD_EVENTS_DATA_FAILURE = 'LOAD_EVENTS_DATA_FAILURE';

// Toggle Action Types
export const TOGGLE_UNIT = 'TOGGLE_UNIT';

// Market Action Types
export const LOAD_PRIMARY_MARKET_SUCCESS = 'LOAD_PRIMARY_MARKET_SUCCESS';
export const LOAD_PRIMARY_MARKET_FAILURE = 'LOAD_PRIMARY_MARKET_FAILURE';
