import React, { Component } from 'react';
import Utility from './../Utility';

class Layout extends Component {
  render() {
    return (
      <Utility>
        <main>{this.props.children}</main>
      </Utility>
    );
  }
}

export default Layout;
