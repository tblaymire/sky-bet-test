import React from 'react';
import Counter from '../../components/UI/Counter/Counter';
import { connect } from 'react-redux';
import * as actions from '../../store/actions/index';

const counterContainer = props => {
	return <Counter countValue={props.events.length} />;
};

const mapStateToProps = state => {
	return {
		events: state.events.events,
	};
};

const mapDispatchToprops = dispatch => {
	return {
		loadEvents: () => dispatch(actions.loadEventsData()),
	};
};

export default connect(mapStateToProps, mapDispatchToprops)(counterContainer);
