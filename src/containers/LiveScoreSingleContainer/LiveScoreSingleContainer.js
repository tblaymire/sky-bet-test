import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from '../../store/actions/index';
import LiveScoreHeader from '../../components/LiveScoreHeader/LiveScoreHeader';

class LiveScoreSingleContainer extends Component {
	componentWillMount() {
		let id = window.location.pathname.match(/football-live\/(\d+)/)[1];
		this.props.loadMarket(id);
	}

	render() {
		return <div>{<LiveScoreHeader event={this.props.singleEvent} />}</div>;
	}
}

const mapStateToProps = state => {
	return {
		markets: state.markets,
	};
};

const mapDispatchToprops = dispatch => {
	return {
		loadMarket: id => dispatch(actions.loadPrimaryMarket(id)),
	};
};

export default connect(mapStateToProps, mapDispatchToprops)(LiveScoreSingleContainer);
