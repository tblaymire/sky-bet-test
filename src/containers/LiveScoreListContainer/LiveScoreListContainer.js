import React, { Component } from 'react';
import LiveScoreList from '../../components/LiveScoreList/LiveScoreList';
import Utility from '../../hoc/Utility';
import { connect } from 'react-redux';
import * as actions from '../../store/actions/index';

class LiveScoreListContainer extends Component {
	constructor() {
		super();

		this.getMarketDetails = this.getMarketDetails.bind(this);

		this.state = {
			singleEventId: null,
			primaryMarketActive: false,
			eventId: null,
		};
	}

	componentDidMount() {
		this.props.loadEvents();
	}

	getMarketDetails = id => {
		this.props.loadMarket(id);
		this.setState({ primaryMarketActive: true });
	};

	render() {
		return (
			<Utility>
				<LiveScoreList
					events={this.props.events}
					markets={this.props.markets}
					eventIdHandler={this.getEventId}
					primaryMarketActive={this.state.primaryMarketActive}
					getMarketDetails={this.getMarketDetails}
					singleEvent={this.props.singleEvent ? this.props.singleEvent : undefined}
				/>
			</Utility>
		);
	}
}

const mapStateToProps = (state, id) => {
	return {
		events: state.events.events,
		markets: state.markets.markets,
	};
};

const mapDispatchToprops = dispatch => {
	return {
		loadEvents: () => dispatch(actions.loadEventsData()),
		loadMarket: id => dispatch(actions.loadPrimaryMarket(id)),
	};
};

export default connect(mapStateToProps, mapDispatchToprops)(LiveScoreListContainer);
