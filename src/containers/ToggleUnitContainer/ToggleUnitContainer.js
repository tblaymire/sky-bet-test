import React, { Component } from 'react';
import ToggleUnit from '../../components/ToggleUnit/ToggleUnit';
import { connect } from 'react-redux';
import * as actions from '../../store/actions/index';

class ToggleUnitContainer extends Component {
	constructor() {
		super();
		this.toggleClickHandler = this.toggleClickHandler.bind(this);
	}

	toggleClickHandler() {
		this.props.updateToggle();

		if (localStorage.getItem('events') === true) {
			console.log('True value stored in localStorage');
		}
	}

	render() {
		return <ToggleUnit toggleClick={this.toggleClickHandler} />;
	}
}

const mapStateToProps = state => {
	return {
		toggleStatus: state.toggleStatus,
	};
};

const mapDispatchToProps = dispatch => {
	return {
		updateToggle: () => dispatch(actions.updateToggleUnit()),
	};
};

export default connect(mapStateToProps, mapDispatchToProps)(ToggleUnitContainer);
