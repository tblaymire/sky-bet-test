import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from '../../store/actions/index';
import Utility from '../../hoc/Utility';
import MarketList from '../../components/Market/MarketList';

class PrimaryMarketContainer extends Component {
	componentWillMount() {}

	render() {
		return (
			<Utility>
				<MarketList markets={this.props.markets} eventId={this.props.id} />
			</Utility>
		);
	}
}

const mapStateToProps = state => {
	return {
		markets: state.markets.markets,
	};
};

const mapDispatchToprops = dispatch => {
	return {
		loadMarket: id => dispatch(actions.loadPrimaryMarket(id)),
	};
};

export default connect(mapStateToProps, mapDispatchToprops)(PrimaryMarketContainer);
