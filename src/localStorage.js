export const loadState = () => {
  try {
    const serializedState = localStorage.getItem('events');
    if (serializedState === null) {
      return undefined;
    }
    return JSON.parse(serializedState);
  } catch (err) {
    return undefined;
  }
};

export const saveState = state => {
  try {
    localStorage.setItem('events', state.events);
  } catch (err) {
    // Ignore write errors.
  }
};
